package modele;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
//Aymerik ABOSO
public class Status {
	private String status;
	private String message;
	
	public Status(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	public Status() {
		this("", "");
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}



















