package modele;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement



public class Voiture {
	private int x;
	private int y;
	private int angle;
	private int vitesse;
	private int acceleration;
	private int rotation;
	private int deceleration;
	private int frein;
	private int vitesseMax;
	
	public Voiture() {
		this(0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	
	public Voiture(int x, int y, int angle, int vitesse, int acceleration, int rotation, int deceleration, int frein,
			int vitesseMax) {
		super();
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.vitesse = vitesse;
		this.acceleration = acceleration;
		this.rotation = rotation;
		this.deceleration = deceleration;
		this.frein = frein;
		this.vitesseMax = vitesseMax;
	}
	
	public void TournerGauche() {
		if(this.vitesse != 0) //On ne tourne pas sur place
			this.angle -= this.rotation;
	}

	public void TournerDroite() {
		if(this.vitesse != 0) //On ne tourne pas sur place
			this.angle += this.rotation;
	}
	
	public void Avancer() {
		this.x = (int)(this.x + Math.cos(this.angle * Math.PI / 180) * this.vitesse);
		this.y = (int)(this.y + Math.sin(this.angle * Math.PI / 180) * this.vitesse);
		this.vitesse += this.acceleration;
		if(this.vitesse > this.vitesseMax)
			this.vitesse = this.vitesseMax;
	}
	
	public void Reculer() {
		this.x = (int)(this.x + Math.cos(this.angle * Math.PI / 180) * this.vitesse);
		this.y = (int)(this.y + Math.sin(this.angle * Math.PI / 180) * this.vitesse);
		this.vitesse = this.vitesse - this.frein; //this.acceleration;
		if(this.vitesse < (-this.vitesseMax))
			this.vitesse = (-this.vitesseMax);
	}
	
	public void RoueLibre() {
		if (this.vitesse > 0) {
			this.x = (int)(this.x + Math.cos(this.angle * Math.PI / 180) * this.vitesse);
			this.y = (int)(this.y + Math.sin(this.angle * Math.PI / 180) * this.vitesse);
			this.vitesse = this.vitesse - this.deceleration;
			if(this.vitesse < 0)
				this.vitesse = 0;
		}
		else if (this.vitesse < 0) {
			this.x = (int)(this.x + Math.cos(this.angle * Math.PI / 180) * this.vitesse);
			this.y = (int)(this.y + Math.sin(this.angle * Math.PI / 180) * this.vitesse);
			this.vitesse = this.vitesse + this.deceleration;
			if(this.vitesse > 0)
				this.vitesse = 0;
		}
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getAngle() {
		return angle;
	}
	
	public void setAngle(int angle) {
		this.angle = angle;
	}
	
	public int getVitesse() {
		return vitesse;
	}
	
	public void setVitesse(int vitesse) {
		this.vitesse = vitesse;
	}
	
	public int getAcceleration() {
		return acceleration;
	}
	
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	
	public int getRotation() {
		return rotation;
	}
	
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}
	
	public int getDeceleration() {
		return deceleration;
	}
	
	public void setDeceleration(int deceleration) {
		this.deceleration = deceleration;
	}
	
	public int getFrein() {
		return frein;
	}
	
	public void setFrein(int frein) {
		this.frein = frein;
	}
	
	public int getVitesseMax() {
		return vitesseMax;
	}
	
	public void setVitesseMax(int vitesseMax) {
		this.vitesseMax = vitesseMax;
	}
}


