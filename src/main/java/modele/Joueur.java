package modele;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

//Aymerik



public class Joueur {
	private String status;
	private String pseudonyme;
	private Voiture voiture;
	private int score;
//Stelly 
	public Joueur() {}
//Aymerik	
	public Joueur(String pseudonyme, Voiture voiture, String status) {
		super();
		this.pseudonyme = pseudonyme;
		this.voiture = voiture;
		this.status = status;
	}

	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public String getPseudonyme() {
		return pseudonyme;
	}

	public void setPseudonyme(String pseudonyme) {
		this.pseudonyme = pseudonyme;
	}

	public Voiture getVoiture() {
		return voiture;
	}

	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
























