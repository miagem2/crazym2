package modele;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

//Aymerik



public class ListeJoueur {
	//Stel modif
	public static ArrayList<Joueur> liste=new ArrayList<Joueur> ();
//Aymerik	
	
	public ListeJoueur() {
		
	}

	/*
	Ajoute un joueur � la liste
	On consid�re que l'ajout d'un m�me joueur n'est pas possible car le cas sera g�r� dans
	le service Nouveau gr�ce � la m�thode rechercherJoueur (pour voir s'il n'est pas d�j� dans la liste)
	*/
	public void ajouter(Joueur j) {
		ListeJoueur.liste.add(j);
	}
	
	/*
	Permet de r�cup�rer un joueur de la liste � partir de son pseudonyme
	Si aucun joueur n'a le pseudonyme cherche, donc n'est pas dans la liste, renvoie null
	*/
	public Joueur rechercherJoueur(String pseudonyme) {
		for(Joueur jListe : ListeJoueur.liste) {
			if(jListe.getPseudonyme().equals(pseudonyme))
				return jListe;
		}
		return null; //Aucun joueur de ce pseudonyme dans la liste
	}
	
	/*
	Supprime un joueur de la liste (s'il existe) qui porte un pseudonyme pr�cis�
	Renvoie true si le joueur a �t� supprim� et faux sinon (pas trouv� dans la liste)
	*/
	public boolean supprimerJoueur(String pseudonyme) {
		int indice = 0;
		boolean trouve = false;
		for(Joueur jListe : ListeJoueur.liste) {
			if(jListe.getPseudonyme().equals(pseudonyme)) {
				trouve = true;
				break;
			}
			indice++;
		}
		if(trouve)
			ListeJoueur.liste.remove(indice);
		return trouve;
	}
//Stell modif 
	public static ArrayList<Joueur> getListe() {
		return liste;
	}
//Aymerik
	public void setListe(ArrayList<Joueur> liste) {
		ListeJoueur.liste = liste;
	}
}



