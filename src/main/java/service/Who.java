package service;

import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import modele.Joueur;
import modele.ListeJoueur;

//Stelly 

@Path("/Who")

public class Who extends HttpServlet {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Joueur> Afficher(){
		if (!ListeJoueur.getListe().isEmpty())
		{
		return ListeJoueur.getListe();
		}
		return null;
	}
	
}
