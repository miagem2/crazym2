package service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import modele.Joueur;
import modele.ListeJoueur;
import modele.Status;
import modele.Voiture;


@Path("/jeu")
public class Jeu {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Joueur> Jeu(@QueryParam("pseudo") String pseudo, @QueryParam("sens") String sens,@QueryParam("angle") String angle) {
		ListeJoueur list = new ListeJoueur();
		Joueur j = list.rechercherJoueur(pseudo);
		if(j != null) {
			if(sens.equals("avancer")) {
				j.getVoiture().Avancer();
			}
			else if(sens.equals("reculer")) {
				j.getVoiture().Reculer();
			}
			else if (sens.equals("libre")) {
				j.getVoiture().RoueLibre();
			}

			if(angle.equals("gauche")) {
				j.getVoiture().TournerGauche();
			}
			else if(angle.equals("droite")) {
				j.getVoiture().TournerDroite();
			}
			else if(angle.equals("rien")) {
				/*
				On ne fait rien pour �viter que la voiture ne ralentissent trop.
				Anciennement il y avait un RoueLibre(), ce qui causait un deuxi�me
				ralentissement (si on avait un "libre" pour le sens)
				*/
			}
		}
		else {
			return null;
		}
		return list.getListe() ;		
	}

	@Path("/DecoJoueur")
	//Aymerik ABOSO
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	/*Pour d�connecter un joueur du serveur (le supprimer)
	Si le joueur de ce pseudo n'existe pas, il ne se passe rien
	 */
	public Status decoJoueur(@QueryParam("pseudo") String pseudo) {
		ListeJoueur listeJoueur = new ListeJoueur();
		boolean supprime = listeJoueur.supprimerJoueur(pseudo);
		Status status = new Status();
		if(supprime) {
			status.setStatus("OK");
			status.setMessage("Joueur ''" + pseudo + "'' deconnecte");
		}
		else {
			status.setStatus("KO");
			status.setMessage("Joueur ''" + pseudo + "'' non deconnecte ou non trouve");
		}
		return status;
	}

	@Path("/DeplacerJoueur")
	//Aymerik ABOSO
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	/*Pour placer un joueur � une position pr�cise avec un angle pr�cis*/
	public Joueur deplacerJoueur(@QueryParam("pseudo")String pseudoJoueur, @QueryParam("x")int x,
			@QueryParam("y")int y, @QueryParam("angle")int angle) {
		ListeJoueur listeJoueur = new ListeJoueur();
		Joueur joueur = listeJoueur.rechercherJoueur(pseudoJoueur);
		if(joueur != null) {
			joueur.getVoiture().setX(x);
			joueur.getVoiture().setY(y);
			joueur.getVoiture().setAngle(angle);
		}
		else {
			Voiture voiture = new Voiture();
			joueur = new Joueur(pseudoJoueur, voiture, "KO");
		}
		return joueur;
	}
	
	@Path("/ModifStats")
	//Aymerik ABOSO
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	/*Donner au client la possibilit� de modifier les caract�ristiques d'une voiture*/
	public Joueur modifierStatistiques(@QueryParam("pseudo")String pseudoJoueur, @QueryParam("x")int x,
			@QueryParam("y")int y, @QueryParam("angle")int angle, @QueryParam("vitesse")int vitesse,
			@QueryParam("acceleration")int acceleration, @QueryParam("rotation")int rotation, 
			@QueryParam("deceleration")int deceleration, @QueryParam("frein")int frein,
			@QueryParam("vitesseMax")int vitesseMax) {
		ListeJoueur listeJoueur = new ListeJoueur();
		Joueur joueur = listeJoueur.rechercherJoueur(pseudoJoueur);
		if(joueur != null) {
			joueur.getVoiture().setX(x);
			joueur.getVoiture().setY(y);
			joueur.getVoiture().setAngle(angle);
			joueur.getVoiture().setVitesse(vitesse);
			joueur.getVoiture().setAcceleration(acceleration);
			joueur.getVoiture().setRotation(rotation);
			joueur.getVoiture().setDeceleration(deceleration);
			joueur.getVoiture().setFrein(frein);
			joueur.getVoiture().setVitesseMax(vitesseMax);
		}
		else {
			Voiture voiture = new Voiture();
			joueur = new Joueur(pseudoJoueur, voiture, "KO");
		}
		return joueur;
	}
}

























