package service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import modele.Joueur;
import modele.ListeJoueur;
import modele.Voiture;

//Stelly 

@Path("/Nouveau")
public class Nouveau {
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//Cr�er nouveau joueur
	
	
	public Joueur AjouterJoueur(@QueryParam("pseudo") String pseudo )
	{	
		//Seb
		ListeJoueur listes=new ListeJoueur();
		//Stel
		Joueur j = listes.rechercherJoueur(pseudo);
		if(j==null)
		{
			Voiture v=new Voiture();
			j=new Joueur(pseudo,v, "OK");
			listes.ajouter(j);
			return j;
		}
		
		return new Joueur(pseudo,new Voiture(), "KO");
	}
	
		
}
